const clickBtn = document.querySelector('.banner-btn');
const banner = document.querySelector('.banner');
const formContainer = document.querySelector('.form-container');
const container = document.querySelector('.container');
const xBtn = document.querySelector('.x-btn')

clickBtn.addEventListener('click', (e)=> {
  banner.style.display = 'none';
  formContainer.style.cssText = 'opacity: 1; visibility: visible';
  container.style.background = '#cc683c';
})

xBtn.addEventListener('click', (e)=> {
  banner.style.display = 'flex';
  formContainer.style.cssText = 'opacity: 0; visibility: hidden';
  container.style.cssText = "linear-gradient(rgba(0,0,0.8), rgba(0,0,0,.7)),url('./img/bg1.jpeg') center no-repeat;background-size:cover";
})